module Main where

import           Control.Monad                (forever, when)
import           Control.Monad.State.Class    (MonadState, get, put)
import           Control.Monad.Trans.Identity (runIdentityT)
import           Control.Monad.Trans.State    (evalStateT, execStateT)
import           Data.Array.IArray            ((!))
import qualified Data.Array.IArray            as Arr
import qualified Data.ByteString              as BS
import           Data.ByteString.Builder
import qualified Data.ByteString.Char8        as BC
import           Data.Function                (on)
import           Data.Functor.Identity        (runIdentity)
import           Data.List                    (maximumBy)
import           Data.Ord                     (comparing)
import           Data.String.Strip
import           Data.Traversable
import           System.Exit                  (exitSuccess)
import           System.IO                    (stdin, stdout)

data LCS where
  LCS :: { _len :: Int
         , _bytes :: Builder
         } -> LCS

instance Show LCS where
  show (LCS l bs) = "len=" ++ show l ++ ", bytes=" ++ (show . toLazyByteString $ bs)

lcs :: BC.ByteString -> BC.ByteString -> LCS
lcs l r = go ll lr
  where
    ll, lr :: Int
    (ll, lr) = ((BS.length l), (BS.length r))

    go :: Int -> Int -> LCS
    go _ 0 = LCS 0 mempty
    go 0 _ = LCS 0 mempty
    go i j
      | (BC.index l (i-1)) == (BC.index r (j-1)) =
          case ds!((i-1), (j-1)) of
            LCS len bytes -> LCS (len+1) (bytes <> char8 (BC.index l (i-1)))

      | otherwise = maximumBy (compare `on` _len) [ds!(i, j-1), ds!(i-1,j)]

    ds :: Arr.Array (Int, Int) LCS
    ds = Arr.listArray bounds [go i j | (i, j) <- Arr.range bounds]

    bounds :: ((Int,Int), (Int,Int))
    bounds = ((0,0), (ll,lr))

main :: IO ()
main = forever $ do
  bytes <- rd
  process bytes
    where
      rd :: IO [BC.ByteString]
      rd = do
        ln <- BS.hGetLine stdin
        return $ BC.words ln

      script :: BC.ByteString -> BC.ByteString -> IO ()
      script a b = do
        let
          res = lcs a b

        BC.hPutStrLn stdout (BC.pack . show $ res)

      process :: [BC.ByteString] -> IO ()
      process ["q"] = exitSuccess
      process ["quit"] = exitSuccess
      process [a] = do
        bytes <- rd
        process (a:bytes)
      process (a:b:ts) = do
        script a b
        process ts
      process _ = return ()
